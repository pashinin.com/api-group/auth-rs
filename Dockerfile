FROM pashinin/rust-runtime:2021-11-18
COPY target/release/auth-http-server ./
# ENTRYPOINT ["/auth-http-server"]
CMD ["/auth-http-server"]
