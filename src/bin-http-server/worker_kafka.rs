//! A worker that executes long-running auth tasks.
//!
//!

use std::thread;
use tokio::runtime::Runtime;
use tokio::time::{sleep, Duration};
use tracing::{info, error};
// use nats::jetstream::Consumer;
use futures::stream::FuturesUnordered;
use futures::{StreamExt, TryStreamExt};
use rdkafka::config::ClientConfig;
use rdkafka::consumer::stream_consumer::StreamConsumer;
use rdkafka::consumer::Consumer;
use rdkafka::message::{BorrowedMessage, OwnedMessage};
use rdkafka::producer::{FutureProducer, FutureRecord};
use rdkafka::Message;


async fn record_borrowed_message_receipt(msg: &BorrowedMessage<'_>) {
    // Simulate some work that must be done in the same order as messages are
    // received; i.e., before truly parallel processing can begin.
    info!("Message received: {}", msg.offset());
}

async fn record_owned_message_receipt(_msg: &OwnedMessage) {
    // Like `record_borrowed_message_receipt`, but takes an `OwnedMessage`
    // instead, as in a real-world use case  an `OwnedMessage` might be more
    // convenient than a `BorrowedMessage`.
}

// Emulates an expensive, synchronous computation.
fn expensive_computation<'a>(msg: OwnedMessage) -> String {
    info!("Starting expensive computation on message {}", msg.offset());
    thread::sleep(Duration::from_millis(rand::random::<u64>() % 5000));
    info!(
        "Expensive computation completed on message {}",
        msg.offset()
    );
    match msg.payload_view::<str>() {
        Some(Ok(payload)) => format!("Payload len for {} is {}", payload, payload.len()),
        Some(Err(_)) => "Message payload is not a string".to_owned(),
        None => "No payload".to_owned(),
    }
}

// Creates all the resources and runs the event loop. The event loop will:
//   1) receive a stream of messages from the `StreamConsumer`.
//   2) filter out eventual Kafka errors.
//   3) send the message to a thread pool for processing.
//   4) produce the result to the output topic.
// `tokio::spawn` is used to handle IO-bound tasks in parallel (e.g., producing
// the messages), while `tokio::task::spawn_blocking` is used to handle the
// simulated CPU-bound task.
async fn run_async_processor(
    brokers: String,
    group_id: String,
    input_topic: String,
    output_topic: String,
) {
    // Create the `StreamConsumer`, to receive the messages from the topic in form of a `Stream`.
    let consumer: StreamConsumer = ClientConfig::new()
        .set("group.id", &group_id)
        .set("bootstrap.servers", &brokers)
        .set("enable.partition.eof", "false")
        .set("session.timeout.ms", "6000")
        .set("enable.auto.commit", "false")
        .create()
        .expect("Consumer creation failed");

    consumer
        .subscribe(&[&input_topic])
        .expect("Can't subscribe to specified topic");

    // Create the `FutureProducer` to produce asynchronously.
    let producer: FutureProducer = ClientConfig::new()
        .set("bootstrap.servers", &brokers)
        .set("message.timeout.ms", "5000")
        .create()
        .expect("Producer creation error");

    // Create the outer pipeline on the message stream.
    let stream_processor = consumer.stream().try_for_each(|borrowed_message| {
        let producer = producer.clone();
        let output_topic = output_topic.to_string();
        async move {
            // Process each message
            record_borrowed_message_receipt(&borrowed_message).await;
            // Borrowed messages can't outlive the consumer they are received from, so they need to
            // be owned in order to be sent to a separate thread.
            let owned_message = borrowed_message.detach();
            record_owned_message_receipt(&owned_message).await;
            tokio::spawn(async move {
                // The body of this block will be executed on the main thread pool,
                // but we perform `expensive_computation` on a separate thread pool
                // for CPU-intensive tasks via `tokio::task::spawn_blocking`.
                let computation_result =
                    tokio::task::spawn_blocking(|| expensive_computation(owned_message))
                        .await
                        .expect("failed to wait for expensive computation");
                let produce_future = producer.send(
                    FutureRecord::to(&output_topic)
                        .key("some key")
                        .payload(&computation_result),
                    Duration::from_secs(0),
                );
                match produce_future.await {
                    Ok(delivery) => println!("Sent: {:?}", delivery),
                    Err((e, _)) => println!("Error: {:?}", e),
                }
            });
            Ok(())
        }
    });

    info!("Starting event loop");
    stream_processor.await.expect("stream processing failed");
    info!("Stream processing terminated");
}

/// Auth worker thread
pub fn create_worker_thread() -> std::thread::JoinHandle<()> {
    thread::spawn( || {
        // Create Tokio runtime
        let rt  = Runtime::new().unwrap();

        // Spawn the root task
        rt.block_on(async {
            let brokers = "kafka:9092";
            let group_id = "example_consumer_group_id";
            let input_topic = "input-topic";
            let output_topic = "output-topic";
            let num_workers = 5;

            (0..num_workers)
                .map(|_| {
                    tokio::spawn(run_async_processor(
                        brokers.to_owned(),
                        group_id.to_owned(),
                        input_topic.to_owned(),
                        output_topic.to_owned(),
                    ))
                })
                .collect::<FuturesUnordered<_>>()
                .for_each(|_| async { () })
                .await

            // let nc = nats::Options::new().connect("jetstream:4222").await.unwrap();
            // let mut consumer: Consumer = nc.create_consumer("my_stream", "my_consumer").await.unwrap();

            // loop {
            //     let msg = consumer.pull().await.unwrap();
            //     tokio::spawn(async move {
            //         println!("got message {:?}", msg.data);
            //         sleep(Duration::from_millis(4000)).await;
            //         println!("4s have elapsed");
            //         msg.ack().unwrap();
            //     });


            //     // let msg = consumer.pull().unwrap();
            //     // tokio::spawn(async move {
            //     //     println!("got message {:?}", msg.data);
            //     //     sleep(Duration::from_millis(4000)).await;
            //     //     println!("4s have elapsed");
            //     //     msg.ack().unwrap();
            //     // });

            //     // let msg_data_len: usize = consumer.process(|msg| {
            //     //     println!("got message {:?}", msg.data);
            //     //     let ten_millis = time::Duration::from_millis(5000);
            //     //     // let now = time::Instant::now();
            //     //     thread::sleep(ten_millis);
            //     //     Ok(msg.data.len())
            //     // }).unwrap();
            // }
        })
    })
}
