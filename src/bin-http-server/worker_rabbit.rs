//! A worker that executes long-running auth tasks.
//!
//!

use std::thread;
use tokio::runtime::Runtime;
use tokio::time::{sleep, Duration};
use tracing::{info, error};
use nats::jetstream::Consumer;
// use futures::stream::FuturesUnordered;
use futures::{TryStreamExt};
use lapin::{
    options::*, publisher_confirm::Confirmation, types::FieldTable, BasicProperties, Connection,
    ConnectionProperties, Result,
};
// use futures_util::stream::StreamExt;
// use tokio::stream::StreamExt;
// use std::iter::Iterator;

/// Auth worker thread
pub fn create_worker_thread() -> std::thread::JoinHandle<()> {
    thread::spawn( || {
        // Create Tokio runtime
        let rt  = Runtime::new().unwrap();

        // Spawn the root task
        rt.block_on(async {
            let addr = std::env::var("AMQP_ADDR").unwrap_or_else(|_| "amqp://rabbitmq:5672/%2f".into());

            // NATS
            // let nc = nats::Options::new().connect("jetstream:4222").unwrap();
            // let mut consumer: Consumer = nc.create_consumer("my_stream", "my_consumer").unwrap();

            // rabbit
            let conn = Connection::connect(
                &addr,
                ConnectionProperties::default(),
            ).await.unwrap();

            info!("CONNECTED");

            // let channel_a = conn.create_channel().await.unwrap();
            let channel_b = conn.create_channel().await.unwrap();

            // let queue = channel_a
            //     .queue_declare(
            //         "hello",
            //         QueueDeclareOptions::default(),
            //         FieldTable::default(),
            //     )
            //     .await.unwrap();

            // info!("Declared queue {:?}", queue);

            let mut consumer = channel_b
                .basic_consume(
                    "hello",
                    "my_consumer",
                    BasicConsumeOptions::default(),
                    FieldTable::default(),
                )
                .await.unwrap();

            tokio::spawn(async move {
                info!("will consume");
                while let Some(delivery) = consumer.next().await {
                    let (channel, delivery) = delivery.expect("error in consumer");
                    println!("received msg: {:?}", delivery);
                    // channel
                    //     .basic_ack(delivery.delivery_tag, BasicAckOptions::default())
                    //     .await.unwrap();
                    // delivery
                    //     .ack(BasicAckOptions::default())
                    //     .await
                    //     .expect("ack");
                }
                info!("WORKER END");
                tokio::time::sleep(tokio::time::Duration::from_millis(100)).await;
            });

            // loop {
            //     // let msg = consumer.pull().await.unwrap();

            //     // tokio::select! {
            //     //     res = consumer.pull() => {
            //     //         let msg = res.unwrap();
            //     //         tokio::spawn(async move {
            //     //             println!("got message {:?}", msg.data);
            //     //             sleep(Duration::from_millis(4000)).await;
            //     //             println!("4s have elapsed");
            //     //             msg.ack().unwrap();
            //     //         });
            //     //     }
            //     //     _ = common::shutdown::graceful_shutdown() => {
            //     //         // The shutdown signal has been received.
            //     //         info!("shutting down worker");
            //     //         break;
            //     //     }
            //     // }




            //     let msg = consumer.pull().unwrap();
            //     tokio::spawn(async move {
            //         println!("got message {:?}", msg.data);
            //         sleep(Duration::from_millis(4000)).await;
            //         println!("4s have elapsed");
            //         msg.ack().unwrap();
            //     });

            //     // let msg_data_len: usize = consumer.process(|msg| {
            //     //     println!("got message {:?}", msg.data);
            //     //     let ten_millis = time::Duration::from_millis(5000);
            //     //     // let now = time::Instant::now();
            //     //     thread::sleep(ten_millis);
            //     //     Ok(msg.data.len())
            //     // }).unwrap();
            // }
        })
    })
}
