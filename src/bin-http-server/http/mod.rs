// use actix_web::middleware::errhandlers::{ErrorHandlers};
use actix_web::{App, HttpServer, web, HttpResponse, middleware};
use passwords::PasswordGenerator;
use std::env;
// use std::{thread, time};
use captcha::Redis as CaptchaStorage;
use common::sql;
use tracing::{info, error};

pub mod endpoints;
pub use endpoints::signup::SignupInput;

#[cfg(not(debug_assertions))]
static DOMAIN_DEFAULT: &'static str = "pashinin.com";

#[cfg(debug_assertions)]
static DOMAIN_DEFAULT: &'static str = "localhost";

#[derive(Clone)]
pub struct AppData {
    vk_client_id: String,
    domain: String,
    password_generator: PasswordGenerator,
    use_social_auth: bool,
}


/// Creates and runs actix-web http server.
#[inline]
pub async fn create_server(
    signup_tx: tokio::sync::mpsc::UnboundedSender<SignupInput>,
) {
    // let dsn = env::var("SENTRY_DSN").unwrap_or(String::from(""));
    // let _guard = sentry::init((dsn, sentry::ClientOptions {
    //     release: sentry::release_name!(),
    //     ..Default::default()
    // }));

    let args = crate::config::args();
    let _conf = crate::config::Config::from(&args);

    // nc.create_stream("my_stream").await.unwrap();

    let port = env::var("NOMAD_PORT_http").unwrap_or(String::from("8080"));
    let addr = format!("0.0.0.0:{}", port);

    info!("{}, {}", env!("CARGO_PKG_NAME"), addr);

    let port = env::var("NOMAD_PORT_http").unwrap_or(String::from("8080"));
    let addr = format!("0.0.0.0:{}", port);

    let pool = match sql::connect().await {
        Ok(pool) => pool,
        Err(_) => {
            error!("Can't connect to sql");
            return
        },
    };

    let storage = auth::Storage { pool };

    #[cfg(debug_assertions)]
    storage.create_debug_user().await;

    // NATS client
    // let nc = nats::Options::new().connect("jetstream:4222").unwrap();

    let mut use_social_auth = false;
    if env::var("VK_CLIENT_ID").unwrap_or("".to_string()) != "" {
        use_social_auth = true;
    }

    // let redis_addr = conf.redis_addr.clone().to_owned();

    HttpServer::new(move || {
        App::new()
            // .wrap(sentry_actix::Sentry::new())
            // .app_data(nc.clone())  // nats::Connection
            //
            .app_data(web::Data::new( signup_tx.clone() ))
            .app_data(web::Data::new( storage.clone() ))
            .app_data(web::Data::new( CaptchaStorage::default()) )
            .app_data(web::Data::new(AppData{
                vk_client_id: env::var("VK_CLIENT_ID").unwrap_or("".to_string()),
                domain: env::var("DOMAIN").unwrap_or(DOMAIN_DEFAULT.to_string()),
                use_social_auth: use_social_auth,
                password_generator: PasswordGenerator {
                    length: 10,
                    numbers: true,
                    lowercase_letters: true,
                    uppercase_letters: true,
                    symbols: true,
                    spaces: false,
                    exclude_similar_characters: true,
                    strict: true,
                }
            }))

            // .wrap(common::session::redis())

            .wrap(common::cors())  // allow requests from subdomains
            .wrap(middleware::Logger::default())

            // .wrap(
            //     ErrorHandlers::new().handler(
            //         http::StatusCode::INTERNAL_SERVER_ERROR,
            //         endpoints::error_500
            //     )
            // )

            .wrap(middleware::Compress::default())

            .service(endpoints::healthcheck)
            .service(endpoints::signin::signin)
            .service(endpoints::signup::signup)

            // .route("/signout", web::get().to(endpoints::manifest))
            .service(endpoints::signout::signout_get)
            .service(endpoints::signout::signout_post)

            .service(
                web::scope("/social/")
                    .service(web::resource("/vk").to(|| async { HttpResponse::Ok() }))
            )
    })
        .bind(addr).unwrap()
        .run()
        .await.unwrap();

    // tokio::select! {
    //     res = http::create_server() => {
    //     }
    //     _ = common::shutdown::graceful_shutdown() => {
    //         // The shutdown signal has been received.
    //         info!("shutting down cluster");
    //     }
    // }
    // use tokio::time::{sleep, Duration};
    // sleep(Duration::from_millis(4000)).await;
}
