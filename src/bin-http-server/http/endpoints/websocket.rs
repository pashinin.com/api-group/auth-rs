use actix::{Actor, StreamHandler, Handler, Message};
use actix_web::{web, HttpResponse, HttpRequest, Error};
use crate::AppData;
use actix::AsyncContext;
use actix_web_actors::ws;
use uuid::Uuid;
use std::str;
use std::time::Duration;
use actix::ActorContext;
use std::time::Instant;

/// How often heartbeat pings are sent
const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
/// How long before lack of client response causes a timeout
const CLIENT_TIMEOUT: Duration = Duration::from_secs(10);

/// Define HTTP actor
// #[derive(Copy)]
pub struct MyWs {
    // nats: nats::Connection,
    // uuid: Uuid,
    // handler: nats::subscription::Handler,
    hb: Instant,
    sub: nats::Subscription,
}

impl MyWs {
    // fn start(&self) {
    // }
}

// #[derive(Message)]
struct ServerEvent {
    event: String,
}
impl Message for ServerEvent {
    type Result = ();
}
impl Handler<ServerEvent> for MyWs {
    type Result = ();

    fn handle(&mut self, msg: ServerEvent, ctx: &mut Self::Context) {
        ctx.text(format!("{}", &msg.event));
    }
}

impl Actor for MyWs {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut ws::WebsocketContext<Self>) {
        let addr = ctx.address();
        // self.sub = Some(self.nats.subscribe(&format!("{}", self.uuid)).unwrap());
        self.sub.clone().with_handler(move |msg| {
            addr.do_send(NatsEvent(msg));
            Ok(())
        });

        // let addr = ctx.address();
        ctx.run_interval(HEARTBEAT_INTERVAL, move |act, ctx| {
            // "act" is MyWs object

            // check client heartbeats
            if Instant::now().duration_since(act.hb) > CLIENT_TIMEOUT {
                // heartbeat timed out
                println!("Websocket Client heartbeat failed, disconnecting!");

                // notify chat server
                // act.addr.do_send(server::Disconnect { id: act.id });

                // stop actor
                ctx.stop();

                // don't try to send a ping
                return;
            }

            // act.addr.do_send(ServerEvent{ event: String::from("Event:") });
            // for l in &act.listeners {
            //     l.do_send(ServerEvent{ event: String::from("Event:") });
            // }
            ctx.ping(b"");
        });
        // self.handler = Some(handler);
    }

    fn stopped(&mut self, _ctx: &mut ws::WebsocketContext<Self>) {
        match self.sub.clone().unsubscribe() {
            Ok(_) => println!("Unsubscribed from NATS: OK"),
            Err(err) => println!("Can't unsubscribe from NATS: {}", err),
        }
    }
}

struct NatsEvent(nats::Message);
impl Message for NatsEvent {
    type Result = ();
}

impl Handler<NatsEvent> for MyWs {
    type Result = ();

    fn handle(&mut self, msg: NatsEvent, ctx: &mut Self::Context) {
        ctx.text(format!("{}", str::from_utf8(&msg.0.data).unwrap()));
    }
}


/// Handler for ws::Message message
impl StreamHandler<Result<ws::Message, ws::ProtocolError>> for MyWs {
    fn handle(
        &mut self,
        msg: Result<ws::Message, ws::ProtocolError>,
        ctx: &mut Self::Context,
    ) {
        match msg {
            Ok(ws::Message::Ping(msg)) => {
                self.hb = Instant::now();
                ctx.pong(&msg);
            },
            Ok(ws::Message::Pong(_)) => {
                self.hb = Instant::now();
            },
            // Ok(ws::Message::Text(text)) => ctx.text(text),
            // Ok(ws::Message::Binary(bin)) => ctx.binary(bin),
            _ => (),
        }
    }
}

pub async fn tree_websocket(
    req: HttpRequest,
    stream: web::Payload,
    web::Path((uuid, )): web::Path<(Uuid, )>,
    data: web::Data<AppData>,
) -> Result<HttpResponse, Error> {
    ws::start(MyWs {
        // nats: data.nats.clone(),
        // uuid: uuid,
        hb: Instant::now(),
        sub: data.nats.subscribe(&format!("{}", uuid)).unwrap(),
        // req: req.clone(),
        // ctx: None,
        // nats: data.nats.subscribe(format!("{}", uuid).as_str()).unwrap(),
        // nats: data.nats.subscribe(&format!("{}", uuid)).unwrap().with_handler(move |msg| {
        //     println!("Received {}", &msg);
        //     // println!("{}", self.uuid);
        //     Ok(())
        // }),
    }, &req, stream)
}
