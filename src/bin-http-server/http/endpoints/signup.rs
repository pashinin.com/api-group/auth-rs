use actix_web::{web, Responder, post, HttpResponse};
// use auth::password;
use auth::storage::Storage;
// use async_std::{task};

// #[cfg(not(debug_assertions))]
use captcha::Redis as CaptchaStorage;
use captcha::Captcha;
use serde::{Serialize, Deserialize};
use std::ops::Deref;
use tracing::info;
// use lapin::options::BasicPublishOptions;
// use lapin::{
//     options::*, publisher_confirm::Confirmation, types::FieldTable, BasicProperties, Connection,
//     ConnectionProperties, Result,
// };

use common::api;
// use crate::mail::send_signup_email;
use crate::http::AppData;


// impl common::api::form::Form for SigninInput {
//     fn errors(&self) -> api::Errors {

//     }
// }
