use actix_web::{Responder, get, HttpResponse};
use serde::{Serialize};
use std::env;
// use crate::AppData;
use auth::User;

#[derive(Serialize)]
struct Info<'a> {
    service: &'a str,
    // version: &'a str,
    tag: &'a str,
    description: &'a str,
    commit_short_sha: &'a str,
    errors: Vec<String>,
}

#[get("/healthcheck")]
pub async fn healthcheck(
    // data: web::Data<AppData>,
    _user: User,
) -> impl Responder {
    let info = Info{
        service: env!("CARGO_PKG_NAME"),
        tag: match option_env!("CI_COMMIT_TAG") {
            Some(tag) => tag,
            None => match option_env!("CI_COMMIT_REF_NAME") {
                Some(branch) => {
                    if branch == "master" {
                        "latest"
                    } else {
                        branch
                    }
                },
                None => "unknown branch",
            },
        },
        description: env!("CARGO_PKG_DESCRIPTION"),
        commit_short_sha: match option_env!("CI_COMMIT_SHORT_SHA") {
            Some(sha) => sha,
            None => "",
        },
        errors: vec![],
    };

    // Check DB connection
    // let _row: (i64,) = match sqlx::query_as("SELECT $1")
    //     .bind(150_i64)
    //     .fetch_one(&data.storage.pool).await {
    //         Ok(data) => data,
    //         Err(err) => {
    //             info.errors.push(err.to_string());
    //             (0, )
    //         }
    //     };

    if info.errors.len() == 0 {
        HttpResponse::Ok().json(info)
    } else {
        HttpResponse::BadRequest().json(info)
    }
}
