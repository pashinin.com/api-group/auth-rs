//! All endpoints used by HTTP-server.
//!
//! /signin
//! /signout

pub mod healthcheck;
pub use healthcheck::*;

pub mod signin;
pub mod signup;
pub mod signout;
