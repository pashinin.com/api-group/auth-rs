//! Signout endpoint
//!
//! JWT logout problem:
//!
//! You cannot manually expire a token after it has been created. Thus,
//! you cannot log out with JWT on the server-side as you do with
//! sessions.
//!
//! In this case when creating a token it is saved to a DB (Redis) with
//! a TTL. This TTL is updated on refreshing a token.
//!
//! Before validating a token - a middleware checks this token is not in
//! blacklist.

use auth::storage::Storage;
use actix_session::Session;
use actix_web::{web, Responder, post, get, HttpResponse, http::{header}};
use common::api;

fn logout(session: Session) {
    // Do not do "session.purge()" as a session cookie will be recreated
    // soon. Just remove it.
    session.remove("user_id");
}

#[get("/signout")]
pub async fn signout_get (
    _user_storage: web::Data<Storage>,
    session: Session,
) -> impl Responder {
    logout(session);
    return HttpResponse::Found()
        .append_header((header::LOCATION, "//tree.pashinin.localhost"))
        .finish();
}


#[post("/signout")]
pub async fn signout_post (
    _user_storage: web::Data<Storage>,
    session: Session,
) -> impl Responder {
    logout(session);
    return HttpResponse::Ok().json(api::Response{ errors: api::Errors::new() });
}
