//! Authentication microservice for [auth.pashinin.com](https://auth.pashinin.com)
//!
//! Functionality:
//!
//! * Registration / signin using email
//! * Account confirmation using email
//! * Sessions using Redis as a backend
//! * Salted and hashed (using Argon2) passwords
//! * Built-in worker for sending emails using NATS
//!
//! While there are some cases to use JWT I use sessions. My reason -
//! ability to revoke a JWT token requires blacklisting a hash of JWT in
//! a central storage. JWT was created to eliminate requests to central
//! storage. All reasons are described
//! [here](http://cryto.net/~joepie91/blog/2016/06/19/stop-using-jwt-for-sessions-part-2-why-your-solution-doesnt-work/).
//!

// use tokio::runtime::Runtime;
// use tracing::{info};
use std::thread;

mod config;
mod http;
use http::SignupInput;

mod worker;
use worker::run_worker;

#[inline]
fn run_http(signup_tx: tokio::sync::mpsc::UnboundedSender<SignupInput>) {
    actix_web::rt::System::new()
        .block_on(async move {
            crate::http::create_server(signup_tx).await;
        });
}

/// This will start http-only, worker-only or both.
fn main() {
    // install global collector configured based on RUST_LOG env var.
    tracing_subscriber::fmt::init();

    let args = config::args();
    let conf = config::Config::from(&args);

    let mut threads = vec![];

    let start_http = args.subcommand_matches("worker").is_none();
    let start_worker = args.subcommand_matches("http").is_none();

    let (signup_tx, signup_rx) = tokio::sync::mpsc::unbounded_channel();

    if start_http && start_worker {
        threads.push(thread::spawn(|| {
            run_http(signup_tx)
        }));
        threads.push(thread::spawn(move || {
            let conf = config::Config::from(&args);
            run_worker(&conf, signup_rx)
        }));

        // Wait for the threads to finish
        for t in threads {
            let _ = t.join();
        }
    } else if start_worker {
        run_worker(&conf, signup_rx);
    } else if start_worker {
        run_http(signup_tx);
    }
}
