
use crate::User;

#[derive(Debug, Clone)]
pub enum AuthenticationInfo {
    // ApiKey {
    //     key: api_key::ApiKeyAuth,
    //     user: User,
    // },
    User(User),
}
