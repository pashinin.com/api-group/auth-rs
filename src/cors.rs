//! CORS (Cross-Origin Resource Sharing)
//!
//! When JS code tries to make requests to some sites those sites
//! must allow these requests.
//!
//! For example api.pashinin.com host must allow JS requests coming from
//! *.pashinin.com hostnames.

use actix_cors::Cors;

#[cfg(not(debug_assertions))]
use actix_web::http;

/// CORS for my services.
///
/// Debug mode - any hostname is allowed to make requests.
///
/// Release mode, allowed:
///   * only requests from .pashinin.com.
///   * GET, POST, OPTIONS methods
pub fn cors() -> Cors {
    // Debug
    #[cfg(debug_assertions)]
    return Cors::permissive();

    // Release
    #[cfg(not(debug_assertions))]
    return Cors::default()
        .allowed_origin("https://pashinin.com")
        .allowed_origin_fn(|origin, _req_head| {
            origin.as_bytes().ends_with(b".pashinin.com")
        })
        .allowed_methods(vec!["GET", "POST", "OPTIONS"])
        .allowed_headers(vec![
            http::header::AUTHORIZATION,
            http::header::ACCEPT,
            http::header::CONTENT_TYPE,
        ])
        .max_age(3600);
}
