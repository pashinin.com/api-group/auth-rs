// use uuid::Uuid;
use serde::{Serialize, Deserialize};
use std::fmt;
// use std::error::Error;


/// Authentication error
#[derive(Debug)]
pub enum Error {
    /// If nothing provided
    EmptyCredentials,

    /// Happens when getting a user from storage.
    ///
    /// Do not use in authentication process!
    UserDoesNotExist,

    /// Can happen on signup
    UserAlreadyExists,

    /// Invalid pair of login/password
    IncorrectCredentials,

    /// Internal error
    InternalError(sqlx::Error),
}

// #[derive(Debug)]
// pub enum SignupError {
//     AlreadyExists,
// }

impl std::error::Error for Error {}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // write!(f, "An Error Occurred, Please Try Again!") // user-facing output
        match *self {
            Error::EmptyCredentials => write!(f, "empty"),
            Error::UserDoesNotExist => write!(f, "UserDoesNotExist"),
            Error::InternalError(ref cause) => write!(f, "DB_Error: {}", cause),
            Error::UserAlreadyExists => write!(f, "UserAlreadyExists"),
            Error::IncorrectCredentials => write!(f, "IncorrectCredentials"),
            // AuthError::Other => write!(f, "Unknown error!"),
        }
    }
}

// Implement std::fmt::Debug for AppError
// impl fmt::Debug for AuthError {
//     fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
//         write!(f, "{{ file: {}, line: {} }}", file!(), line!()) // programmer-facing output
//     }
// }



// impl ResponseError trait allows to convert our errors into http responses with appropriate data
// impl ResponseError for ServiceError {
//     fn error_response(&self) -> HttpResponse {
//         match self {
//             ServiceError::InternalServerError => {
//                 HttpResponse::InternalServerError().json("Internal Server Error, Please try later")
//             }
//             ServiceError::BadRequest(ref message) => HttpResponse::BadRequest().json(message),
//             ServiceError::JWKSFetchError => {
//                 HttpResponse::InternalServerError().json("Could not fetch JWKS")
//             }
//         }
//     }
// }


#[derive(Serialize, Deserialize)]
pub struct SignupErrors {
    pub login: Option<String>,
    pub captcha: Option<String>,
}
