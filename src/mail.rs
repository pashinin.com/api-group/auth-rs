//! Compose and send authentication mails
//!
//!

use std::fmt;
use std::error::Error;
use lettre::transport::smtp;
use tracing::{info};
// use lettre::transport::smtp::authentication::Credentials;

#[derive(Debug)]
pub enum MailError {
    ComposeError(lettre::error::Error),
    SendError(smtp::Error)
}

impl Error for MailError {}

impl fmt::Display for MailError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            MailError::ComposeError(ref cause) => write!(f, "ComposeError: {}", cause),
            MailError::SendError(ref cause) => write!(f, "SendError: {}", cause),
        }
    }
}


#[cfg(debug_assertions)]
pub async fn send_signup_email(
    plain_password: &str,
    to: &str,
) -> Result<(), MailError> {
    info!("DEBUG mode: NOT sending signup email for: {}, password: {}", to, plain_password);
    Ok(())
}

/// Signup email
#[cfg(not(debug_assertions))]
pub async fn send_signup_email(
    plain_password: &str,
    to: &str,
) -> Result<(), MailError> {
    // Imports are here because of different release/debug versions of a
    // function
    use uuid::Uuid;
    use lettre::{
        Message, SmtpTransport, Transport,
        // transport::stub::StubTransport,
        message::{header, MultiPart, SinglePart},
    };
    let domain = "pashinin.com";
    info!("Sending signup email for: {}, password: {}", to, plain_password);

    // TODO: use Askama template
    let body = format!(
        "Добро пожаловать на {}

Логин: {}
Пароль: {}

",
        domain,
        to,
        plain_password,
    );
    let email = match Message::builder()
        .from("robot@pashinin.com".parse().unwrap())
        .to(to.parse().unwrap())
        .subject(format!("Добро пожаловать на {}", domain))
        .message_id(Some(format!("<{}@{}>", Uuid::new_v4(), "pashinin.com")))
        .multipart(
            MultiPart::alternative()
                .singlepart(
                    SinglePart::builder()
                        // Content-Type: text/plain; charset=utf-8
                        .header(header::ContentType::TEXT_PLAIN)
                        .body(body),
                )
        ) {
            Ok(mail) => mail,
            Err(err) => {
                return Err(MailError::ComposeError(err));
            },
        };
    // let creds = Credentials::new("smtp_username".to_string(), "smtp_password".to_string());

    // Open a remote connection to gmail
    // let mailer = SmtpTransport::relay("smtp.gmail.com").unwrap().credentials(creds).build();
    let mailer = SmtpTransport::relay("pashinin.com").unwrap().build();

    // Send the email
    match mailer.send(&email) {
        Ok(_res) => Ok(()),
        Err(err) => Err(MailError::SendError(err))
    }
}
