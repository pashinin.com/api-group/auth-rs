//! CLI auth functions

#[cfg(not(debug_assertions))]
use std::io::Write;

use crate::storage::SigninInput;

#[cfg(not(debug_assertions))]
pub fn read_creadentials() -> SigninInput {
    let mut login = String::new();
    print!("Login: ");
    std::io::stdout().flush().unwrap();
    std::io::stdin().read_line(&mut login).ok().expect("Failed to read line");
    let password = rpassword::prompt_password("Password: ").unwrap();

    SigninInput {
        login: login.to_string(),
        password: password.to_string(),
    }
}

#[cfg(debug_assertions)]
pub fn read_creadentials() -> SigninInput {
    SigninInput {
        login: "admin@localhost".to_string(),
        password: "password123#".to_string(),
    }
}
