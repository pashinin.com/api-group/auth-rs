//! By design, domain names must have at least two dots; otherwise the
//! browser will consider them invalid. (See reference on
//! http://curl.haxx.se/rfc/cookie_spec.html).
//!

use actix_session::{SessionMiddleware, storage::RedisActorSessionStore};
use actix_session::config::PersistentSession;
use actix_web::cookie::Key;
use std::env;
use std::str::FromStr;
use tracing::{error, info};
use crate::storage::SigninInput;

pub fn redis_connection_string() -> String {
    env::var("REDIS").unwrap_or(crate::config::REDIS_ADDRESS_DEFAULT.into())
}

/// The secret key would usually be read from a configuration
/// file/environment variables.
pub fn session_secret_key() -> Key {
    // let secret_key = Key::generate();
    Key::from(&[0; 64])
}

/// Session middleware (debug)
///
/// When working on localhost, the cookie domain must be omitted
/// entirely!
#[cfg(debug_assertions)]
pub fn middleware() -> SessionMiddleware<RedisActorSessionStore> {
    SessionMiddleware::builder(
        RedisActorSessionStore::new(redis_connection_string()),
        session_secret_key()
    )
        .session_lifecycle(
            PersistentSession::default()
                .session_ttl(time::Duration::days(5)))
        .cookie_domain(Some("pashinin.localhost".to_string()))
        .build()
}


/// Session middleware (release)
#[cfg(not(debug_assertions))]
pub fn middleware() -> SessionMiddleware<RedisActorSessionStore> {
    SessionMiddleware::builder(
        RedisActorSessionStore::new(redis_connection_string()),
        session_secret_key()
    )
    .session_lifecycle(
        PersistentSession::default()
            .session_ttl(time::Duration::days(5)))
    .cookie_http_only(true)
    .cookie_domain(Some("pashinin.com".to_string()))
    .cookie_secure(true)  // via https only
    .build()
}



/// Authoorizes and gets user session
///
/// Helper function used in CLI utils.
pub async fn auth_session(input: &SigninInput) -> Result<String, String> {
    info!("Making auth request to {}", crate::config::AUTH_URL);
    let client = reqwest::Client::builder()
        .cookie_store(true)
        .build().unwrap();
    let data = serde_json::to_string(input).unwrap();

    let res = match client.post(format!("{}/api/signin", crate::config::AUTH_URL))
        .body(data.clone())
        .header(reqwest::header::CONTENT_TYPE, "application/json")
        .send()
        .await {
            Ok(r) => r,
            Err(e) => {
                error!("Can't autorize: {}", e);
                return Err(e.to_string());
            }
        };

    // dbg!(res.text().await.unwrap());
    // dbg!(res);

    let cookie_str = match res.headers().get("set-cookie") {
        Some(v) => v.to_str().unwrap(),
        None => {
            return Err("No set-cookie header".to_string());
        }
    };
    let c = cookie::Cookie::from_str(cookie_str).unwrap();
    info!("Got new session");
    Ok(c.value().to_string())
    // "asd".to_string()
}


/// Gets user session and updates a session file.
///
/// Helper function used in CLI utils.
pub async fn get_session(force: bool) -> Result<String, String> {
    let mut session_file = std::path::PathBuf::new();
    session_file.push(std::env::var("HOME").unwrap());

    #[cfg(debug_assertions)]
    session_file.push(".pashinin-dev");

    #[cfg(not(debug_assertions))]
    session_file.push(".pashinin");


    if force {
        let session = auth_session(&crate::cli::read_creadentials())
            .await?;
        if let Err(e) = std::fs::write(session_file.as_path(), session.as_bytes()) {
            error!("Can't write session to {}: {}", session_file.display(), e);
        }
        return Ok(session);
    }

    match std::fs::read_to_string(session_file.as_path()) {
        Ok(content) => Ok(content),
        Err(_) => {
            info!("No file {}", session_file.display());
            let session = match auth_session(&crate::cli::read_creadentials()).await {
                Ok(session) => session,
                Err(e) => {
                    error!("Can't auth: {}", e);
                    return Err(e);
                }
            };
            if let Err(e) = std::fs::write(session_file.as_path(), session.as_bytes()) {
                error!("Can't write session to {}: {}", session_file.display(), e);
            }
            Ok(session)
        }
    }
}
