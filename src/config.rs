
#[cfg(debug_assertions)]
pub static AUTH_URL: &str = "http://auth.pashinin.localhost";

#[cfg(not(debug_assertions))]
pub static AUTH_URL: &str = "https://auth.pashinin.com";


#[cfg(not(debug_assertions))]
pub static REDIS_ADDRESS_DEFAULT: &'static str = "localhost:6379";

#[cfg(debug_assertions)]
pub static REDIS_ADDRESS_DEFAULT: &'static str = "redis:6379";
