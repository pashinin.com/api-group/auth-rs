//! User
//!
//! Do not forget to pass a storage to your app.
//!
//! ```
//! use actix_web::{get, App, web, HttpServer, Responder};
//!
//! #[get("/")]
//! async fn index(
//!     // path: web::Path<(u32, String)>
//! ) -> impl Responder {
//!     format!("Hello ")
//! }
//!
//! #[actix_web::main]
//! async fn main() {
//!     HttpServer::new(move || {
//!         App::new()
//!             .service(web::resource("/").to(|| async { "asd" }))
//!     });
//! }
//! ```

use actix_web::{HttpRequest, Error, FromRequest, dev, web};
use actix_session::SessionExt;  // req.get_session()
use core::future::Future;
use serde::{Serialize};
use std::fmt;
use uuid::Uuid;
use std::pin::Pin;
use tracing::{info, error};

use crate::storage::Storage;


/// User
///
/// `User::default()` - anonymous user
///
#[derive(Serialize,sqlx::FromRow,Default,Debug,Clone)]
pub struct User {
    pub id: Uuid,
    pub first_name: String,
    pub last_name: String,
    pub email: String,
    pub lng: String,

    /// User's files "folder"
    pub files_root_id: Option<Uuid>,

    #[serde(skip)] // Remove password from JSON
    pub password: String,

    #[serde(skip)]
    pub updated_at: Option<chrono::DateTime<chrono::Utc>>,
}

impl User {
    // pub async fn storage_get_self(&mut self, storage: &Storage) -> Tree {
    //     storage.get_user(self).await
    // }

    // pub fn url(&self) -> String {
    //     format!("/profile/?uuid={}", self.id)
    // }

    #[inline]
    pub fn is_authenticated(&self) -> bool {
        !self.id.is_nil() && self.updated_at.is_some()
    }
}


impl FromRequest for User {
    type Error = Error;
    type Future = Pin<Box<dyn Future<Output = Result<Self, Self::Error>>>>;

    fn from_request(req: &HttpRequest, _payload: &mut dev::Payload) -> Self::Future {
        // If you forget to pass a Storage - log an error and just
        // return an anonymous user.
        let storage = match req.app_data::<web::Data<Storage>>() {
            Some(storage) => storage.clone(),
            None => {
                error!("You forgot to pass auth::UserStorage into your Actix app.");
                return Box::pin(async move {
                    return Ok(User::default())
                })
            }
        };

        let session = req.get_session();

        Box::pin(async move {
            match session.get::<Uuid>("user_id") {
                Ok(Some(uuid)) => {
                    info!("Got user from session: {}", uuid);
                    match storage.get_user_by_uuid(uuid).await {
                        Ok(user) => Ok(user),
                        Err(err) => {
                            error!("Can't fetch user by UUID: {}", err);
                            Ok(User::default())
                        }
                    }
                },
                Ok(None) => {
                    info!("No 'user_id' in a current session");
                    Ok(User::default())
                },
                Err(e) => {
                    error!("session error: {}", e);
                    Ok(User::default())
                },
            }
        })
    }
}

impl PartialEq for User {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}


impl fmt::Display for User {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.id)
    }
}

#[derive(Debug,sqlx::Type)]
pub enum SigninResult {
    Fail = 0,
    Ok = 1,
}

/// Information saved on each auth attempt
pub struct AuthAttempt<'a> {
    /// Empty ("") if successfully logged in
    pub login_used: &'a str,

    /// None if auth attempt is failed
    pub user_uuid: Option<Uuid>,

    // TODO: IP address

    /// Fail = 0
    /// Success = 1
    pub result: SigninResult,

    pub time: chrono::DateTime<chrono::Utc>,
}
