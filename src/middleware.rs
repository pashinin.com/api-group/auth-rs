//! Auth middleware

use actix_web::dev::{
    Service, ServiceRequest, ServiceResponse,
    // Transform,
};
// use actix_web::HttpMessage;
use std::rc::Rc;
// use crate::auth_info::AuthenticationInfo;
use futures::{
    future::{
        // ready,
        LocalBoxFuture,
        // Ready,
    },
    FutureExt,
};

pub struct AuthenticateMiddleware<S> {
    auth_data: Rc<crate::storage::Storage>,
    service: Rc<S>,
}

impl<S, B> Service<ServiceRequest> for AuthenticateMiddleware<S>
where
    S: Service<ServiceRequest, Response = ServiceResponse<B>, Error = actix_web::Error> + 'static,
{
    type Response = ServiceResponse<B>;
    type Error = actix_web::Error;
    type Future = LocalBoxFuture<'static, Result<Self::Response, Self::Error>>;

    actix_service::forward_ready!(service);

    fn call(&self, req: ServiceRequest) -> Self::Future {
        let srv = Rc::clone(&self.service);
        let _auth_data = self.auth_data.clone();

        async move {
            // let id = req.get_identity();
            // let auth = auth_data.authenticate(id, &req).await?;
            // if let Some(auth) = auth {
            //     req.extensions_mut()
            //         .insert::<Rc<AuthenticationInfo>>(Rc::new(auth));
            // }

            let res = srv.call(req).await?;

            Ok(res)
        }
        .boxed_local()
    }
}
