use sqlx;
use uuid::Uuid;
use serde::{Serialize, Deserialize};
use crate::user::{User, SigninResult};
use crate::error::Error as AuthError;
use hashbrown::HashMap;
// use captcha::Captcha;
// use crate::password;
// use actix_web::middleware::session::SessionStorage;
// use actix_redis::RedisSession;
use common::api;
use tracing::error;

static USER_FIELDS: &'static str = "id, first_name, last_name, email,
password, updated_at, lng, files_root_id";


/// Data provided by the user while registering
#[derive(Serialize, Deserialize, Debug)]
pub struct ProfileInput {
    // pub id: Uuid,
    pub first_name: String,
    pub last_name: String,
}

#[derive(Default, Serialize, Deserialize)]
pub struct SigninInput {
    /// Login can be only email for now
    pub login: String,
    pub password: String,
}


impl SigninInput {
    /// Returns true if something (login or password) is empty.
    #[inline]
    pub fn is_empty(&self) -> bool {
        self.login.is_empty() || self.password.is_empty()
    }
}


impl common::api::form::Form for SigninInput {
    fn errors(&self) -> api::Errors {
        // let hm = HashMap::<String, String>::new();
        let mut hm = HashMap::new();
        if self.login.is_empty() {
            hm.insert("login", "empty".to_string());
        }
        if self.password.is_empty() {
            hm.insert("password", "empty".to_string());
        }
        api::Errors {
            errors: hm,
        }
        // if hm.len() > 0 {
        //     Some(hm)
        // } else {
        //     None
        // }
    }
}


#[derive(Clone)]
pub struct Storage {
    pub pool: sqlx::Pool<sqlx::Postgres>

    // pub fn new(pool: &sqlx::Pool<sqlx::Postgres>) -> Self {
    //     Storage {
    //     }
    // }

    // pool: Option<Storage>,
}


impl Storage {
    // /// Creates a storage and connects to it.
    // pub async fn new(&self) -> Result<User, AuthError> {
    //     storage::Storage {
    //         pool: sql::connect().await?,
    //     }
    // }

    #[cfg(debug_assertions)]
    pub async fn create_debug_user(&self) {
        let email = "admin@localhost";
        let password = "password123#";
        let password_hash = crate::password::hash_encoded(password.as_bytes());  // 0.5s
        if self.get_user_by_email(email).await.is_err() {
            self.create_new_user(email, &password_hash).await.unwrap();
        }
    }

    /// Get User from a database if a password is correct.
    ///
    /// Returns Err if user does not exist or password is incorrect.
    ///
    /// Must NOT return UserDoesNotExist error for security reasons.
    pub async fn authenticate(&self, input: &SigninInput) -> Result<User, AuthError> {
        if input.login == "" || input.password == "" {
            return Err(AuthError::EmptyCredentials);
        }

        // 1. Check if user exists
	    let user: User = match self.get_user_by_email(&input.login).await {
            Ok(user) => user,
            Err(err) => {
                #[cfg(debug_assertions)]
                error!("{} {}", err, input.login);

                match err {
                    AuthError::UserDoesNotExist => return Err(AuthError::IncorrectCredentials),
                    _ => return Err(err),
                }
            },
        };

        // What error can happen?
        //
        // In case of incorrect data is in the "password" field then
        // "Decoding failed" error will happen.
        let matches = match argon2::verify_encoded(&user.password, &input.password.as_bytes()) {
            Ok(ok) => ok,
            Err(e) => {
                error!("{}", e);
                false
            }
        };

        if matches {
            Ok(user)
        } else {
            Err(AuthError::IncorrectCredentials)
        }
    }

    /// UPDATE user info
    pub async fn save_user(
        &self,
        user: &User,
    ) -> bool {
        sqlx::query("UPDATE users SET
first_name=$2,
last_name=$3,
files_root_id=$4
WHERE id=$1")
            .bind(&user.id)
            .bind(&user.first_name)
            .bind(&user.last_name)
            .bind(&user.files_root_id)
            // .bind(&new_info.files_root_id)
            .execute(&self.pool).await
            .map(|user| user)
            .map_err(|e| {
                error!("{}", e);
                match e {
                    sqlx::Error::Database(ref err) => {
                        if let Some(code) = err.code() {
                            if code == "23505" {
                                return AuthError::UserAlreadyExists
                            }
                        }
                    }
                    _ => {},
                };
                AuthError::InternalError(e)
            }).unwrap();
        true
    }

    /// UPDATE user info
    pub async fn update_user(
        &self,
        user: &User,
        new_info: &ProfileInput,
    ) -> bool {
        sqlx::query("UPDATE users SET
first_name=$2,
last_name=$3
WHERE id=$1")
            .bind(&user.id)
            .bind(&new_info.first_name)
            .bind(&new_info.last_name)
            .execute(&self.pool).await
            .map(|user| user)
            .map_err(|e| {
                error!("{}", e);
                match e {
                    sqlx::Error::Database(ref err) => {
                        if let Some(code) = err.code() {
                            if code == "23505" {
                                return AuthError::UserAlreadyExists
                            }
                        }
                    }
                    _ => {},
                };
                AuthError::InternalError(e)
            }).unwrap();
        true
    }

    /// INSERT a new user into storage.
    pub async fn create_new_user(
        &self,
        email: &str,
        password_hash: &str
    ) -> Result<User, AuthError> {
        sqlx::query_as(&format!(
            "INSERT INTO users
(first_name, last_name, email, password)
VALUES ($1, $2, $3, $4)
RETURNING {}", USER_FIELDS))
            .bind("first")
            .bind("last")
            .bind(email)
            .bind(password_hash)
            .fetch_one(&self.pool).await
            .map(|user| user)
            .map_err(|e| {
                match e {
                    sqlx::Error::Database(ref err) => {
                        if let Some(code) = err.code() {
                            if code == "23505" {
                                return AuthError::UserAlreadyExists
                            }
                        }
                    }
                    _ => {},
                };
                AuthError::InternalError(e)
            })
    }

    /// Get User object by email.
    /// Gets latest record since there are many records for 1 user.
    pub async fn get_user_by_email(&self, email: &str) -> Result<User, AuthError> {
        match sqlx::query_as(
            &format!("SELECT {} FROM users WHERE email=$1 ORDER BY updated_at DESC LIMIT 1", USER_FIELDS))
            .bind(email)
            .fetch_one(&self.pool).await {
                Ok(user) => return Ok(user),
                Err(err) => {
                    match err {
                        sqlx::Error::RowNotFound => return Err(AuthError::UserDoesNotExist),
                        _ => return Err(AuthError::InternalError(err))
                    }
                },
            };
    }

    /// Log auth attempt
    pub async fn auth_log(&self, login: &str, msg: SigninResult) {
        match sqlx::query("INSERT INTO auth_log (login_used, message_type) VALUES ($1, $2)")
            .bind(login)
            .bind(msg as i16)
            .execute(&self.pool).await {
                Ok(_) => {},
                Err(err) => println!("{}", err),
            }
    }

    /// Get User object by UUID
    pub async fn get_user_by_uuid(&self, uuid: Uuid) -> Result<User, AuthError> {
        match sqlx::query_as(
            &format!("SELECT {} FROM users WHERE id=$1", USER_FIELDS))
            .bind(uuid)
            .fetch_one(&self.pool).await {
                Ok(user) => Ok(user),
                Err(err) => {
                    match err {
                        sqlx::Error::RowNotFound => Err(AuthError::UserDoesNotExist),
                        _ => Err(AuthError::InternalError(err))
                    }
                },
            }
    }
}
