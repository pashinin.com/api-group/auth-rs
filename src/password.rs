//! Argon2i is used for password hashing.
//!
//! Default settings:
//!
//! ```ignore
//! hash_length = 32
//! lanes = 1
//! mem_cost = 4096
//! time_cost = 3
//! ```


use rand::Rng;
use argon2::{self, Config};

/// Hash plain password using Argon2i
pub fn hash_encoded(password: &[u8]) -> String {
    // 16 random bytes
    let salt = rand::thread_rng().gen::<[u8; 16]>();
    let config = Config::default();
    let hash = argon2::hash_encoded(password, &salt, &config).unwrap();
    // let matches = argon2::verify_encoded(&hash, password).unwrap();
    // assert!(matches);
    hash
}
